/* 
    Authors    : Tiffany Holden, Micah Strube, Joel Braun
    VirusTotal Group
*/

// Toggle used when search results are displayed to prevent
// the table from automatically repopulating with the full
// file list.
var search_displayed = false;

function maliciousFileSearch() {
    if($('#searchbox').val() != "") {
        $.ajax({
            type: "GET",
            url:"api/1/search",
            success: (data) => {
                console.dir(data);
                search_displayed = true;
                $("#exitSearchButton").show();
                gd_success(data);
                vt_success(data);
            },
            error: re_auth,
            data: {searchterm: $("#searchbox").val()},
        });
    }
}

function vtResults() {
    if(!search_displayed) {
        $.ajax({
            url: "api/1/files/scans",
            success: vt_success,
            error: re_auth,
            dataType: "json"
        });
    }
}

function vt_link_cell(vt_result) {
    let link_div = document.createElement('div');
    // If file was scanned and is malicious
    if (vt_result.scanned == 1 && vt_result.positiveScanResults >0) {
        let maliciousLink = document.createElement('a');
        maliciousLink.className = 'ScanMalicious fa fa-bug';
        maliciousLink.href = vt_result.permalink;
        maliciousLink.target = "blank";
        link_div.appendChild(maliciousLink);
    }
    // If file was scanned and is not malicious
    else if (vt_result.scanned == 1 && vt_result.positiveScanResults <1) {
        let maliciousLink = document.createElement('a');
        maliciousLink.className = 'ScanClean fa fa-check-circle';
        maliciousLink.href = vt_result.permalink;
        maliciousLink.target = "blank";
        link_div.appendChild(maliciousLink);
    }
    // If file scan is in progress
    else if (vt_result.scanned == -1) {
        let maliciousLink = document.createElement('div');
        maliciousLink.className = 'ScanInProgress fa fa-spinner fa-pulse fa-fw';
        link_div.appendChild(maliciousLink);
    }
    // If file was not scanned
    else {
        let scanLink = document.createElement('a');
        scanLink.className = 'Unscanned fa fa-search-plus';
        scanLink.onclick = function() {
            vt_scan_content(vt_result);
            let scanLink = document.createElement('div');
            scanLink.className = 'ScanInProgress fa fa-spinner fa-spin';
        };
        link_div.appendChild(scanLink);
    }

    return link_div;
}

function vt_scan_content(vt_result) {
    var file = {fileId: vt_result.fileId};

    $.ajax({
        url: "api/1/file/scan",
        type: "PUT",
        success: {},
        dataType: "json",
        contentType: 'application/json',
        data: JSON.stringify(file)
    });
}

function vt_scan_date_cell(vt_result) {
    let date_div = document.createElement('div');
    if (vt_result.scanned == 1) {
        date_div.innerText = vt_result.scanDate;
    } else if (vt_result.scanned == -1) {
        date_div.innerText = "Scan In Progress...";
    } else {
        date_div.innerText = "Unscanned";
    }
    return date_div;
}

function vt_success(scanData) {
    console.dir("SCAN DATA:");
    console.dir(scanData);

    for (var i = 0; i < scanData.length; i++) {
        let scan;
        if(!search_displayed) {
            scan = scanData[i];
        }
        else {
            scan = scanData[i].scan;
        }

        let vt_file = $('#' + scan.fileId);

        /* On first pass for each result (hasResults == false), add the
         * scan result link cell, and the scan date cell.
         *
         * On the second pass (once hasResults != false), update the
         * existing cells.
         */
        if (vt_file.hasClass("hasResults") == false) {
            let link_cell = document.createElement('td');
            let link_cell_id = scan.fileId + '-linkcell';
            let date_cell = document.createElement('td');
            let date_cell_id = scan.fileId + '-datecell';

            link_cell.setAttribute('id', link_cell_id);
            vt_file.append(link_cell);

            date_cell.setAttribute('id', date_cell_id);
            vt_file.append(date_cell);

            vt_file.addClass("hasResults");
        }

        let link_cell = $('#' + scan.fileId + '-linkcell');
        link_cell.empty().append(vt_link_cell(scan));

        let date_cell = $('#' + scan.fileId + '-datecell');
        date_cell.empty().append(vt_scan_date_cell(scan));
    }

    setTimeout(vtResults, 2000);
}

function is_logged_in() {
    $.ajax({
        url: "api/1/user/loggedin",
        success: logged_data,
        dataType: "json"
    });
}

function fa_icon(fa_type) {
    let fa_element = document.createElement('i');
    fa_element.className = "fas " + fa_type;
    return fa_element;
}

function load_files() {
    $.ajax({
        url: "api/1/files",
        success: (data) => {
          gd_success(data);
        },
        error: re_auth,
        dataType: "json"
    });
}

function re_auth() {
    window.location = '/authorize';
}

function exit_search_results() {
    $('#exitSearchButton').hide();
    search_displayed = false;
}

function logged_data(loginStatus) {
    if (loginStatus.loggedin == 1) {
        load_files();
    } else {
        /* User is not logged in show them register button */
        let loginButton = document.createElement('button');
        loginButton.id = "connectGDrive";
        loginButton.innerHTML = "<i class='fab fa-google-drive'></i> Connect Google Drive";
        $(loginButton).click(function(e) {
            e.preventDefault();
            window.location = "authorize";
            is_logged_in();
        });
        $('#gdriveBtn').html(loginButton);
    }
}

function gd_success(driveData) {
    console.dir("DRIVE DATA:");
    console.dir(driveData);
    var rowData = "";
    for (var i = 0; i < driveData.length; i++) {
        console.dir("Adding to table: " + driveData[i].fileId);
        rowData += "<tr id=" + driveData[i].fileId + "><td>";
        rowData += driveData[i].fileName + '</td>';
        rowData += '<td>' + driveData[i].mimeType + '</td></tr>';
    }
    $('#data').html(rowData);
    setTimeout(vtResults, 1000);
}

function run_on_load() {
    /* add functions here to run on page load */
    $('#exitSearchButton').hide();
    $('#exitSearchButton').click(function() {
      exit_search_results();
    });
    is_logged_in();
    $("#searchButton").click((e) => {
        e.stopPropagation();
        e.preventDefault();
        maliciousFileSearch();
    });
}

window.onload = run_on_load;
